import { FETCH_WEATHER } from '../actions/index';

export default function(state = [], action) {
    // console.log('Action recieved', action);
    switch(action.type) {
        case FETCH_WEATHER:
            // // Use the .concat() method rather than the .push() method to add new data to the state
            // return state.push(action.payload.data);
            // return state.concat([ action.payload.data ]);

            // This approach works too.
            return [ action.payload.data, ...state ];
    }

    return state;
}
